﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Colliderscope {
    public class TracksCollidersOverInstantiation : MonoBehaviour {

        public static Dictionary<int, Collider> Colliders { get; } = new Dictionary<int, Collider>();
        public static Dictionary<int, SaveableDataHolder> Blocks { get; } = new Dictionary<int, SaveableDataHolder>();

        static System.Random random = new System.Random();

        public int hash;
        public int order;

        public int OnBuildCreate(SaveableDataHolder block, int order) {
            hash = GetHashCode();
            if (Colliders.ContainsKey(hash)) {
                hash = Mod.CustomHash(hash, random.Next());
            }
            this.order = order;
            Colliders.Add(hash, null);
            Blocks.Add(hash, block);
            return hash;
        }
        public void OnSimCreate() {
            Colliders[hash] = GetComponents<Collider>()[order];
            Destroy(this);
        }

        public void CleanUp() {
            Colliders.Remove(hash);
            Blocks.Remove(hash);
        }

    }
}