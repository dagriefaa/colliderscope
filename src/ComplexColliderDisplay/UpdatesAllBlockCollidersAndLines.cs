﻿using Modding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Colliderscope {
    public class UpdatesAllBlockCollidersAndLines : MonoBehaviour {

        public SaveableDataHolder block;
        public List<LineRenderer> JointLineRenderers { get; private set; } = new List<LineRenderer>();
        List<Material> jointLineMaterials = new List<Material>();
        static Camera mainCamera = null;

        Transform colliderParent;
        Transform jointsParent;

        public static Dictionary<KeyCode, HashSet<BlockBehaviour>> EmulatorKeys = new Dictionary<KeyCode, HashSet<BlockBehaviour>>();
        public static Dictionary<string, HashSet<BlockBehaviour>> EmulatorMessages = new Dictionary<string, HashSet<BlockBehaviour>>();

        public static Dictionary<KeyCode, HashSet<BlockBehaviour>> ListenerKeys = new Dictionary<KeyCode, HashSet<BlockBehaviour>>();
        public static Dictionary<string, HashSet<BlockBehaviour>> ListenerMessages = new Dictionary<string, HashSet<BlockBehaviour>>();

        public HashSet<KeyCode> localEmulatorKeys = new HashSet<KeyCode>();
        public HashSet<string> localEmulatorMessages = new HashSet<string>();

        public HashSet<KeyCode> localListenerKeys = new HashSet<KeyCode>();
        public HashSet<string> localListenerMessages = new HashSet<string>();

        public List<GameObject> KeyLineRenderers { get; private set; } = new List<GameObject>();

        void Awake() {
            if (!mainCamera)
                mainCamera = Camera.main;
            colliderParent = transform.FindChild("Colliders Vis");
            // we want the massive lag spike when turning it on in sim to be when we start simulation, not in the middle of it
            colliderParent?.gameObject.SetActive(true);

            block = GetComponent<SaveableDataHolder>();

            if (block is BlockBehaviour) {
                jointsParent = this.transform.FindChild("Joints Vis") ?? new GameObject("Joints Vis").transform;
                jointsParent.SetParent(transform, false);
            }

            if (colliderParent && block.isSimulating) {
                block.GetComponentsInChildren<TracksCollidersOverInstantiation>(true).ToList().ForEach(x => x.OnSimCreate());
            }
            else if (block is BlockBehaviour && !ReferenceMaster.activeMachineSimulating) {
                block.KeyList.ForEach(x => { x.KeysChanged += UpdateKeyReferences; });
            }
        }

        IEnumerator Start() {
            UpdateKeyReferences();

            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;

            if (ReferenceMaster.activeMachineSimulating && block is BlockBehaviour) {
                GetComponentsInChildren<Joint>().ToList().ForEach(MakeJointLine);
            }

            yield break;
        }

        void Update() {
            PreserveScale();

            if (block is BlockBehaviour) {
                colliderParent.gameObject.SetActive(ColliderscopeController.ShowColliders
                    && (ColliderscopeController.ShowBlockSimulation || !block.isSimulating));
            }
            if (block is GenericEntity) {
                colliderParent.gameObject.SetActive((ColliderscopeController.ShowEntitySimulation || !block.isSimulating)
                    && ColliderscopeController.IsLevelEditor);
            }
        }

        void OnDestroy() {
            jointLineMaterials.ForEach(x => Destroy(x));
            RemoveOldKeys(true);
            if (block && !block.isSimulating) {
                GetComponentsInChildren<TracksCollidersOverInstantiation>(true).ToList().ForEach(y => y.CleanUp());
            }
        }

        void PreserveScale() {
            if (colliderParent.lossyScale != Vector3.one || colliderParent.lossyScale != Vector3.zero) {
                colliderParent.localScale = new Vector3(
                    transform.localScale.x == 0 ? 0f : 1f / transform.localScale.x,
                    transform.localScale.y == 0 ? 0f : 1f / transform.localScale.y,
                    transform.localScale.z == 0 ? 0f : 1f / transform.localScale.z);
            }
        }

        public static float GetLineWidth(Vector3 position, float baseSize = 0.005f, float min = 0.00001f, float max = 0.07f) {
            Plane plane = new Plane(mainCamera.transform.forward, mainCamera.transform.position);
            float distanceToPoint = plane.GetDistanceToPoint(position);
            float d = mainCamera.fieldOfView / 41f;
            float scale = Mathf.Abs(baseSize * distanceToPoint * d);
            return Mathf.Clamp(scale, min, max);
        }

        void MakeKeyLine(BlockBehaviour b, bool inBackground) {
            GameObject lineParent = new GameObject("KeyLine");
            lineParent.transform.parent = jointsParent;
            lineParent.layer = (inBackground) ? 23 : 0;
            KeyLineRenderers.Add(lineParent);
            lineParent.AddComponent<LineRenderer>();
            RendersKeyLine controller = lineParent.AddComponent<RendersKeyLine>();
            controller.from = block as BlockBehaviour;
            controller.to = b;
            controller.inBackground = inBackground;
        }

        void MakeJointLine(Joint j) {
            GameObject lineParent = new GameObject("JointLine");
            lineParent.transform.parent = jointsParent;
            lineParent.layer = 23;
            lineParent.AddComponent<LineRenderer>();
            lineParent.AddComponent<RendersJointLine>().joint = j;
        }

        public void UpdateKeyRendering() {
            if (block is GenericEntity || ReferenceMaster.activeMachineSimulating) {
                return;
            }

            KeyLineRenderers.FindAll(x => x).ForEach(Destroy);
            KeyLineRenderers.Clear();

            foreach (MKey key in (block as BlockBehaviour).KeyList) {
                if (key.isEmulator) {
                    continue;
                }

                if (key.useMessage) {
                    foreach (string name in key.message) {
                        EmulatorMessages.TryGetValue(name, out HashSet<BlockBehaviour> b);
                        b?.ToList().ForEach(x => {
                            MakeKeyLine(x, true);
                            MakeKeyLine(x, false);
                        });
                    }
                }
                else {
                    for (int i = 0; i < key.KeysCount; i++) {
                        EmulatorKeys.TryGetValue(key.GetKey(i), out HashSet<BlockBehaviour> b);
                        b?.ToList().ForEach(x => {
                            MakeKeyLine(x, true);
                            MakeKeyLine(x, false);
                        });
                    }
                }
            }
        }

        void UpdateKeyReferences() {
            RemoveOldKeys();
            AddNewKeys();
            UpdateKeyRendering();
        }

        void AddNewKeys() {
            if (block is GenericEntity || ReferenceMaster.activeMachineSimulating) {
                return;
            }

            if (!block) {
                return;
            }

            foreach (MKey key in (block as BlockBehaviour).KeyList) {

                if (key.useMessage) {
                    Dictionary<string, HashSet<BlockBehaviour>> messageDict = (key.isEmulator) ? EmulatorMessages : ListenerMessages;
                    HashSet<string> localMessageList = (key.isEmulator) ? localEmulatorMessages : localListenerMessages;

                    localMessageList.Clear();
                    localMessageList.UnionWith(key.message);

                    foreach (string name in localMessageList) {
                        if (string.IsNullOrEmpty(name)) {
                            continue;
                        }
                        if (!messageDict.ContainsKey(name)) {
                            messageDict[name] = new HashSet<BlockBehaviour>();
                        }
                        if (!messageDict[name].Add(block as BlockBehaviour)) {
                            continue;
                        }

                        if (key.isEmulator && ListenerMessages.ContainsKey(name)) {
                            ListenerMessages[name].ToList().FindAll(x => !x).ForEach(x => Debug.Log($"{name} has null listener"));
                            ListenerMessages[name].ToList().FindAll(x => x).ForEach(x => x?.GetComponent<UpdatesAllBlockCollidersAndLines>().UpdateKeyRendering());
                        }
                    }
                }
                else {
                    Dictionary<KeyCode, HashSet<BlockBehaviour>> keyDict = (key.isEmulator) ? EmulatorKeys : ListenerKeys;
                    HashSet<KeyCode> localKeyList = (key.isEmulator) ? localEmulatorKeys : localListenerKeys;

                    localKeyList.Clear();
                    for (int i = 0; i < key.KeysCount; i++) {
                        if (key.GetKey(i) == KeyCode.None) {
                            continue;
                        }
                        localKeyList.Add(key.GetKey(i));
                    }

                    foreach (KeyCode code in localKeyList) {
                        if (!keyDict.ContainsKey(code)) {
                            keyDict[code] = new HashSet<BlockBehaviour>();
                        }
                        if (!keyDict[code].Add(block as BlockBehaviour)) {
                            continue;
                        }
                        if (key.isEmulator && ListenerKeys.ContainsKey(code)) {
                            ListenerKeys[code].ToList().FindAll(x => !x).ForEach(x => Debug.Log($"{code} has null listener"));
                            ListenerKeys[code].ToList().FindAll(x => x).ForEach(x => x?.GetComponent<UpdatesAllBlockCollidersAndLines>().UpdateKeyRendering());
                        }
                    }
                }
            }
        }

        void RemoveOldKeys(bool isDestroyed = false) {
            if (block is GenericEntity || ReferenceMaster.activeMachineSimulating) {
                return;
            }

            if (!block) {
                return;
            }

            foreach (MKey key in (block as BlockBehaviour).KeyList) {

                Dictionary<KeyCode, HashSet<BlockBehaviour>> keyDict = (key.isEmulator) ? EmulatorKeys : ListenerKeys;
                Dictionary<string, HashSet<BlockBehaviour>> messageDict = (key.isEmulator) ? EmulatorMessages : ListenerMessages;

                HashSet<string> localMessageList = (key.isEmulator) ? localEmulatorMessages : localListenerMessages;
                HashSet<KeyCode> localKeyList = (key.isEmulator) ? localEmulatorKeys : localListenerKeys;

                if (isDestroyed) {
                    if (key.useMessage) {
                        localMessageList.UnionWith(key.message);
                    }
                    else {
                        for (int i = 0; i < key.KeysCount; i++) {
                            localKeyList.Add(key.GetKey(i));
                        }
                    }
                }

                foreach (string name in localMessageList) {
                    if (!messageDict.ContainsKey(name)) {
                        continue;
                    }
                    messageDict[name].Remove(block as BlockBehaviour);
                    messageDict[name].RemoveWhere(x => !x);
                    if (messageDict[name].Count == 0) {
                        messageDict.Remove(name);
                    }
                    if (key.isEmulator && ListenerMessages.ContainsKey(name)) {
                        ListenerMessages[name].ToList().FindAll(x => x).ForEach(x => x?.GetComponent<UpdatesAllBlockCollidersAndLines>().UpdateKeyRendering());
                    }
                }
                localMessageList.Clear();

                foreach (KeyCode code in localKeyList) {
                    if (!keyDict.ContainsKey(code)) {
                        continue;
                    }
                    keyDict[code].Remove(block as BlockBehaviour);
                    keyDict[code].RemoveWhere(x => !x);
                    if (keyDict[code].Count == 0) {
                        keyDict.Remove(code);
                    }
                    if (key.isEmulator && ListenerKeys.ContainsKey(code)) {
                        ListenerKeys[code].ToList().FindAll(x => x).ForEach(x => x?.GetComponent<UpdatesAllBlockCollidersAndLines>().UpdateKeyRendering());
                    }
                }
                localKeyList.Clear();
            }
        }
    }
}