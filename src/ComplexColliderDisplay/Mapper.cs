﻿using Colliderscope.Types;
using ObjectExplorer.Mappings;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Colliderscope {

    public class Mapper : MonoBehaviour {

        void Awake() {

            foreach (AbstractType t in ColliderscopeController.BlockColliderTypes.Values) {
                ObjectExplorer.ObjectExplorer.AddMappings("Colliderscope",
                    new MColor<ColliderscopeController>(t.Name, c => t.ColliderColor, (c, x) => t.ColliderColor = x)
                );
            }

            ObjectExplorer.ObjectExplorer.AddMappings("Colliderscope",

                new MInt<ColliderscopeController>("MaxJobsPerFrame", c => ColliderscopeController.MaxJobsPerFrame,
                    (c, x) => ColliderscopeController.MaxJobsPerFrame = x),

                new MInt<ColliderscopeController>("CreateJobs", c => ColliderscopeController.CreateJobCount),
                new MInt<ColliderscopeController>("DestroyJobs", c => ColliderscopeController.DestroyJobCount),
                new MIterable<ColliderscopeController, string>("EmulatorKeys",
                    c => UpdatesAllBlockCollidersAndLines.EmulatorKeys.Select(
                        x => $"{x.Key}: {string.Join(",", x.Value.Select(y => $"{y?.name} {y?.transform.GetSiblingIndex().ToString() ?? ""}").ToArray())}")
                    .ToList()
                ),
                new MIterable<ColliderscopeController, string>("EmulatorMessages",
                    c => UpdatesAllBlockCollidersAndLines.EmulatorMessages.Select(
                        x => $"{x.Key}: {string.Join(",", x.Value.Select(y => $"{y?.name} {y?.transform.GetSiblingIndex().ToString() ?? ""}").ToArray())}")
                    .ToList()
                ),
                new MIterable<ColliderscopeController, string>("ListenerKeys",
                    c => UpdatesAllBlockCollidersAndLines.ListenerKeys.Select(
                        x => $"{x.Key}: {string.Join(",", x.Value.Select(y => $"{y?.name} {y?.transform.GetSiblingIndex().ToString() ?? ""}").ToArray())}")
                    .ToList()
                ),
                new MIterable<ColliderscopeController, string>("ListenerMessages",
                    c => UpdatesAllBlockCollidersAndLines.ListenerMessages.Select(
                        x => $"{x.Key}: {string.Join(",", x.Value.Select(y => $"{y?.name} {y?.transform.GetSiblingIndex().ToString() ?? ""}").ToArray())}")
                    .ToList()
                ),
                new MIterable<ColliderscopeController, string>("ColliderMap", c => TracksCollidersOverInstantiation.Colliders.Select(x => $"{x.Key}: {x.Value}").ToList()),
                new MIterable<ColliderscopeController, string>("BlockMap", c => TracksCollidersOverInstantiation.Blocks.Select(x => $"{x.Key}: {x.Value}").ToList()),

                new MString<UpdatesSingleColliderVisuals>("VisType", c => c.VisType?.Name),
                new MComponent<UpdatesSingleColliderVisuals, Collider>("VisCollider", c => c.Self),
                new MComponent<UpdatesSingleColliderVisuals, SaveableDataHolder>("VisBlock", c => c.Block),

                new MInt<TracksCollidersOverInstantiation>("hash", c => c.hash),

                new MIterable<UpdatesAllBlockCollidersAndLines, LineRenderer>("JointLineRenderers", c => c.JointLineRenderers),
                new MIterable<UpdatesAllBlockCollidersAndLines, KeyCode>("EmulatorKeys", c => c.localEmulatorKeys),
                new MIterable<UpdatesAllBlockCollidersAndLines, string>("EmulatorMessages", c => c.localEmulatorMessages),

                new MComponent<RendersKeyLine, BlockBehaviour>("From", c => c.from),
                new MComponent<RendersKeyLine, BlockBehaviour>("To", c => c.to),

                new MComponent<RendersJointLine, Rigidbody>("JointConnectedBody", c => c.joint.connectedBody)
            ); ;

            Destroy(this);
        }
    }

}
