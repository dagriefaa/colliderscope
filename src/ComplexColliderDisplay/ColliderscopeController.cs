﻿using Colliderscope.Types;
using Colliderscope.UI;
using Modding;
using Modding.Blocks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Colliderscope {
    public class ColliderscopeController : MonoBehaviour {

        public const string COLLIDER_PARENT_NAME = "Colliders Vis";

        private static bool? _showColliders = null;
        public static bool ShowColliders {
            get {
                if (_showColliders == null) {
                    if (Configuration.GetData().HasKey("ShowColliders")) {
                        ShowColliders = Configuration.GetData().ReadBool("ShowColliders");
                    }
                    else {
                        ShowColliders = true;
                    }
                }
                return (bool)_showColliders;
            }
            set {
                _showColliders = value;
                Configuration.GetData().Write("ShowColliders", value);
            }
        }
        private static bool? _showBlockSimulation = null;
        public static bool ShowBlockSimulation {
            get {
                if (_showBlockSimulation == null) {
                    if (Configuration.GetData().HasKey("ShowBlockSimulation")) {
                        ShowBlockSimulation = Configuration.GetData().ReadBool("ShowBlockSimulation");
                    }
                    else {
                        ShowBlockSimulation = true;
                    }
                }
                return (bool)_showBlockSimulation;
            }
            set {
                _showBlockSimulation = value;
                Configuration.GetData().Write("ShowBlockSimulation", value);
            }
        }
        private static bool? _showEntitySimulation = null;
        public static bool ShowEntitySimulation {
            get {
                if (_showEntitySimulation == null) {
                    if (Configuration.GetData().HasKey("ShowEntitySimulation")) {
                        ShowEntitySimulation = Configuration.GetData().ReadBool("ShowEntitySimulation");
                    }
                    else {
                        ShowEntitySimulation = true;
                    }
                }
                return (bool)_showEntitySimulation;
            }
            set {
                _showEntitySimulation = value;
                Configuration.GetData().Write("ShowEntitySimulation", value);
            }
        }

        private static bool? _xRay = null;
        public static bool XRay {
            get {
                if (_xRay == null) {
                    if (Configuration.GetData().HasKey("XRay")) {
                        XRay = Configuration.GetData().ReadBool("XRay");
                    }
                    else {
                        XRay = true;
                    }
                }
                return (bool)_xRay;
            }
            set {
                _xRay = value;
                Configuration.GetData().Write("XRay", value);
            }
        }

        private static bool? _showJoints = null;
        public static bool ShowJoints {
            get {
                if (_showJoints == null) {
                    if (Configuration.GetData().HasKey("ShowJoints")) {
                        ShowJoints = Configuration.GetData().ReadBool("ShowJoints");
                    }
                    else {
                        ShowJoints = true;
                    }
                }
                return (bool)_showJoints;
            }
            set {
                _showJoints = value;
                Configuration.GetData().Write("ShowJoints", value);
            }
        }

        public static int MaxJobsPerFrame = 15;

        /*  NonConnectableSolid, // layer 0, child
         *  NonConnectableNonSolid, // layer 21, 16, layer 0 directly attached
         *  ConnectableSolid, // layer 12, 14 
         *  ConnectableNonSolid, // layer 12, 14 with property isTrigger
         *  SurfaceSolid,
         *  PrimaryTrigger, // layer 22
         *  SecondaryTrigger, // component TriggerSetJoint2 
         *  MechanicalTrigger, // layer 22 with mechanicalTag
         *  StickyTrigger, // component TriggerSetJointBrace with property canJoinMultiple
         *  EnvironmentTrigger // layer 2
         */
        public static Dictionary<string, AbstractType> BlockColliderTypes = new Dictionary<string, AbstractType>();
        public static Dictionary<string, AbstractType> EntityColliderTypes = new Dictionary<string, AbstractType>();

        public static bool IsLevelEditor = false;

        struct BuildPair { public SaveableDataHolder b; public GameObject o; };
        static Queue<BuildPair> jobs = new Queue<BuildPair>();
        public static int CreateJobCount => jobs.Count;
        public static int DestroyJobCount => UpdatesSingleColliderVisuals.TrashList.Count;

        static bool jobRunning = false;
        static int jobFrameCount = 0;

        static ColliderscopeController Instance = null;

        GameObject buttonParent = null;

        void Awake() {
            DontDestroyOnLoad(gameObject);
            Instance = this;

            Events.OnBlockInit += x => AddColliderVis(x.InternalObject);
            Events.OnEntityPlaced += x => AddColliderVis(x.InternalObject.EntityBehaviour);
            Events.OnLevelLoaded += x => x.Entities.ToList().ConvertAll(y => y.InternalObject.EntityBehaviour).ForEach(y => AddColliderVis(y));

            BlockColliderTypes["SurfaceSolid"] = new SurfaceSolid();
            BlockColliderTypes["NonConnectableSolid"] = new NonConnectableSolid();
            BlockColliderTypes["NonConnectableNonSolid"] = new NonConnectableNonSolid();
            BlockColliderTypes["ConnectableSolid"] = new ConnectableSolid();
            BlockColliderTypes["ConnectableNonSolid"] = new ConnectableNonSolid();
            BlockColliderTypes["EnvironmentTrigger"] = new EnvironmentTrigger();
            BlockColliderTypes["SecondaryTrigger"] = new SecondaryTrigger();
            BlockColliderTypes["StickyTrigger"] = new StickyTrigger();
            BlockColliderTypes["MechanicalTrigger"] = new MechanicalTrigger();
            BlockColliderTypes["PrimaryTrigger"] = new PrimaryTrigger();

            BlockColliderTypes.Values.ToList().ForEach(x => ReferenceMaster.onMachineModified += x.OnUpdate);

            EntityColliderTypes["EntityTrigger"] = new EntityTrigger();
            EntityColliderTypes["EntityDecal"] = new EntityDecal();
            EntityColliderTypes["EntitySolid"] = new EntitySolid();

            Events.OnActiveSceneChanged += (x, y) => MakeUI();
            if (StatMaster.isMP) {
                MakeUI();
            }
        }

        private void MakeUI() {
            if (Mod.SceneNotPlayable())
                return;
            Transform settings = GameObject.Find("HUD").transform.FindChild("TopBar/Align (Top Right)/SettingsContainer/Settings (Fold Out)");
            Transform god = settings.FindChild("GOD");
            god.localPosition += new Vector3(0, -0.4f, 0);
            Transform bg = god.FindChild("BG");
            bg.localPosition += new Vector3(0, 0.2f, 0);
            bg.localScale += new Vector3(0, -0.4f, 0);

            buttonParent = new GameObject("ColliderButtons");
            buttonParent.transform.SetParent(settings.FindChild("MISC"), false);

            GameObject clusterButtonObj = settings.FindChild("MISC/CLUSTER").gameObject;

            // make collider vis button
            GameObject colliderButtonObj = (GameObject)Instantiate(clusterButtonObj, clusterButtonObj.transform.parent, true);
            colliderButtonObj.transform.localPosition += new Vector3(0, -0.375f, 0);
            colliderButtonObj.name = "COLLIDER";

            ToggleClusterHighlight oldButton = colliderButtonObj.transform.FindChild("CLUSTER BUTTON").GetComponent<ToggleClusterHighlight>();
            if (!Switch.redMaterial)
                Switch.redMaterial = oldButton.redMaterial;
            if (!Switch.darkMaterial)
                Switch.darkMaterial = oldButton.darkMaterial;
            Destroy(oldButton);

            Switch colliderButton = colliderButtonObj.transform.FindChild("CLUSTER BUTTON").gameObject.AddComponent<Switch>();
            colliderButton.get = () => ShowColliders;
            colliderButton.set = x => { ShowColliders = x; buttonParent.SetActive(x); };
            Transform textObject = colliderButtonObj.transform.FindChild("CLUSTER text");
            DestroyImmediate(textObject.GetComponent<Localisation.LocalisationChild>());
            textObject.GetComponent<TextMesh>().text = "COLLIDER";

            // make visual mode buttons
            MakeButton(colliderButtonObj, new Vector3(1, -2.5f, 1), "IN SIM",
                () => ShowBlockSimulation, x => ShowBlockSimulation = x, true, null);
            MakeButton(colliderButtonObj, new Vector3(1, -1.5f, 1), "X-RAY",
                () => !XRay, x => XRay = !x, true, null);

            // make collider type buttons
            foreach (AbstractType c in BlockColliderTypes.Values) {
                MakeButton(colliderButtonObj, c.ButtonOffset, c.Label, () => c.Enabled, x => c.Enabled = x);
            }

            // make joint display button
            MakeButton(colliderButtonObj, new Vector3(-1, 0, 1), "JOINTS",
                () => ShowJoints, x => ShowJoints = x, true, buttonParent.transform.parent);

            buttonParent.SetActive(ShowColliders);

            // level collider switches
            if (StatMaster.isMP) {
                Transform parent = GameObject.Find("HUD").transform.FindChild("Snap left/LevelEditor/Container/Categories");
                if (!(IsLevelEditor = parent)) {
                    return;
                }
                GameObject eButtonParent = new GameObject("ColliderButtons");
                eButtonParent.transform.SetParent(parent, false);
                eButtonParent.transform.localPosition = new Vector3(5.3f, -8.8f, 0);
                eButtonParent.transform.localScale = new Vector3(1.666f, 1.852f, 1.666f);

                MakeButton(colliderButtonObj, new Vector3(3, 0, 0) * 1.05f, "IN SIM",
                    () => ShowEntitySimulation, x => ShowEntitySimulation = x, true, eButtonParent.transform);

                foreach (AbstractType c in EntityColliderTypes.Values) {
                    MakeButton(colliderButtonObj, c.ButtonOffset * 1.05f, c.Label, () => c.Enabled, x => c.Enabled = x,
                        true, eButtonParent.transform);
                }
            }
        }

        void MakeButton(GameObject original, Vector3 offset, string name, Func<bool> get, Action<bool> set,
            bool center = false, Transform parent = null, bool relative = true) {
            GameObject buttonObj = (GameObject)Instantiate(original, parent ?? buttonParent.transform, false);
            if (relative) {
                buttonObj.transform.localPosition += Vector3.Scale(offset, new Vector3(-0.75f, -0.375f, 0));
            }
            else {
                buttonObj.transform.localPosition = Vector3.Scale(offset, new Vector3(-0.75f, -0.375f, 0));
            }
            buttonObj.name = name;
            Switch button = buttonObj.GetComponentInChildren<Switch>();
            button.get = get;
            button.set = set;
            TextMesh text = buttonObj.GetComponentInChildren<TextMesh>();
            text.text = name;
            if (!center) {
                text.anchor = TextAnchor.MiddleLeft;
                text.alignment = TextAlignment.Left;
                text.transform.localPosition += new Vector3(-0.35f, 0);
            }
        }

        private void AddColliderVis(SaveableDataHolder b) {
            if (b is BlockBehaviour && (b as BlockBehaviour).ParentMachine != Machine.Active()) { // not player's machine
                return;
            }
            if (b is GenericEntity && !IsLevelEditor) { // no level editor
                return;
            }
            if (b is BuildZoneObject || b is SpawnZoneEntity || b is InsigniaTriggerInvisible) { // entities that already have collider vis objects
                return;
            }

            // vis objects already exist and will be retrieved later
            if (b is BlockBehaviour && ReferenceMaster.activeMachineSimulating
                || b is GenericEntity && StatMaster.levelSimulating) {
                return;
            }

            GameObject colliderParent = MakeColliderParent(b.transform);
            if (b is SensorBlock) {
                b.gameObject.AddComponent<ShowsSensorAoE>();
            }
            if (b is BuildSurface) {
                b.gameObject.AddComponent<UpdatesSurfaceCollidersOnChange>();
            }
            if (b is FlamethrowerController) {
                b.gameObject.AddComponent<ShowsFlamethrowerAoE>();
            }
            StartAddColliderBuild(b, colliderParent);
        }

        private static GameObject MakeColliderParent(Transform t) {
            GameObject colliderParent = new GameObject(COLLIDER_PARENT_NAME);
            colliderParent.transform.parent = t;
            colliderParent.transform.localPosition = Vector3.zero;
            colliderParent.transform.localRotation = Quaternion.identity;
            colliderParent.transform.localScale = Vector3.one;
            return colliderParent;
        }

        private void StartAddColliderBuild(SaveableDataHolder block, GameObject colliderParent) {
            jobs.Enqueue(new BuildPair { b = block, o = colliderParent });

            if (!jobRunning) {
                StartCoroutine(AddColliderBuild());
            }
        }

        private IEnumerator AddColliderBuild() {
            jobRunning = true;

            while (jobs.Count > 0) {
                BuildPair bp = jobs.Dequeue();
                SaveableDataHolder block = bp.b;
                GameObject colliderParent = bp.o;

                // if block doesn't exist at time of execution
                if (!block) {
                    continue;
                }

                // clean up existing children
                List<Transform> toClean = new List<Transform> { colliderParent.transform };

                if (block is BuildSurface) {
                    block.GetComponentsInChildren<Collider>(true)
                        .Select(x => x.transform.parent)
                        .Where(x => x.name.StartsWith("Surface Fragment"))
                        .Select(x => x.FindChild(COLLIDER_PARENT_NAME));
                }
                if (block is ShorteningBlock) {
                    toClean.Add(block.transform.FindChild("BrokenVisBottom/" + COLLIDER_PARENT_NAME));
                }
                foreach (Transform t in toClean) {
                    if (!t) { continue; }
                    for (int i = 0; i < t.childCount; i++) {
                        GameObject toTrash = t.GetChild(i).gameObject;
                        toTrash.SetActive(false);
                        UpdatesSingleColliderVisuals.TrashList.Enqueue(toTrash);
                    }
                }

                // generate new colliders
                int collidersOnObject = 0;
                GameObject currentObject = null;
                foreach (Collider c in block.GetComponentsInChildren<Collider>(true)) {
                    if (!block || !c)
                        break;

                    Modding.Serialization.ModCollider colliderVisGenerator;
                    Transform colliderVis;

                    if (c.gameObject.layer == 8) {
                        continue;
                    }

                    // generate visuals
                    if (c is BoxCollider) {
                        colliderVisGenerator = new Modding.Serialization.BoxModCollider() { Position = new Vector3(0, -1000, 0) };
                    }
                    else if (c is SphereCollider) {
                        colliderVisGenerator = new Modding.Serialization.SphereModCollider() {
                            Position = new Vector3(0, -1000, 0),
                            Radius = (c as SphereCollider).radius
                        };
                    }
                    else if (c is CapsuleCollider) {
                        colliderVisGenerator = new Modding.Serialization.CapsuleModCollider() {
                            Capsule = new Modding.Serialization.CapsuleModCollider.CapsuleWrapper() {
                                Radius = 1f, // dummy
                                Height = 3f, // dummy
                                Direction = (Modding.Serialization.Direction)(c as CapsuleCollider).direction
                            }
                        };
                    }
                    else {
                        continue; // mesh collider
                    }

                    Transform parentTransform = colliderParent.transform;
                    // reparenting colliders
                    if (block is BuildSurface && c.transform.parent.name.StartsWith("Surface Fragment")) { // surface fragments
                        parentTransform = c.transform.parent.FindChild(COLLIDER_PARENT_NAME)
                            ?? MakeColliderParent(c.transform.parent).transform;
                        if (!parentTransform.parent.GetComponent<ScalesSurfaceFragments>()) {
                            parentTransform.parent.gameObject.AddComponent<ScalesSurfaceFragments>();
                        }
                    }
                    else if (block is ShorteningBlock && c.name == "BrokenVisBottom") { // log fragment
                        parentTransform = c.transform.FindChild(COLLIDER_PARENT_NAME)
                            ?? MakeColliderParent(c.transform).transform;
                        if (!parentTransform.parent.GetComponent<ScalesSurfaceFragments>()) {
                            parentTransform.parent.gameObject.AddComponent<ScalesSurfaceFragments>();
                        }
                    }

                    if (currentObject != c.gameObject) {
                        currentObject = c.gameObject;
                        collidersOnObject = 0;
                    }

                    int colliderHash = c.gameObject.AddComponent<TracksCollidersOverInstantiation>().OnBuildCreate(block, collidersOnObject);
                    collidersOnObject++;

                    colliderVis = colliderVisGenerator.CreateVisual(transform);
                    colliderVis.SetParent(parentTransform, false);
                    UpdatesSingleColliderVisuals.Make(colliderVis.gameObject, c, block, colliderHash);

                    colliderVis.name = $"{c.name} ({colliderVis.name})";

                    colliderVis.rotation = c.transform.rotation;

                    // handle capsule direction
                    if (c is CapsuleCollider) {
                        switch ((Modding.Serialization.Direction)(c as CapsuleCollider).direction) {
                            case Modding.Serialization.Direction.X:
                                colliderVis.localRotation *= Quaternion.Euler(0f, 90f, 0f);
                                break;
                            case Modding.Serialization.Direction.Y:
                                colliderVis.localRotation *= Quaternion.Euler(90f, 0f, 0f);
                                break;
                        }
                    }

                    // yield once limit reached
                    if (++jobFrameCount >= MaxJobsPerFrame) {
                        jobFrameCount = 0;
                        yield return null;
                    }
                }
                if (!block) {
                    continue;
                }
                if (block is BuildSurface) {
                    block.GetComponent<UpdatesSurfaceCollidersOnChange>().FinishSetup();
                }
                if (!block.GetComponent<UpdatesAllBlockCollidersAndLines>()) {
                    block.gameObject.AddComponent<UpdatesAllBlockCollidersAndLines>();
                }
            }

            jobRunning = false;
            yield break;
        }

        public static void RemakeColliderVis(Block b) {
            Transform t = b.InternalObject.transform.FindChild(COLLIDER_PARENT_NAME);
            Instance.StartAddColliderBuild(b.InternalObject, t.gameObject);
        }

        void Update() {
            for (int i = 0; i < 30; i++) {
                if (UpdatesSingleColliderVisuals.TrashList.Count == 0) {
                    break;
                }
                GameObject o = UpdatesSingleColliderVisuals.TrashList.Dequeue();
                if (!o) {
                    continue;
                }
                Destroy(o);
            }
        }
    }
}