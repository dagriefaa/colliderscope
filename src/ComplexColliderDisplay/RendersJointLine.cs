﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Colliderscope {
    class RendersJointLine : MonoBehaviour {

        public Joint joint;

        Material material;
        LineRenderer self;
        float strength;
        BlockBehaviour blockBehaviour;

        void Start() {
            self = GetComponent<LineRenderer>();
            self.material = material = new Material(Shader.Find("Particles/Alpha Blended"));
            blockBehaviour = GetComponentInParent<BlockBehaviour>();
            strength = joint.breakForce;
        }

        void OnDestroy() {
            Destroy(material);
        }

        void Update() {

            if (!joint || !joint.connectedBody || !self) {
                self.SetColors(Color.clear, Color.clear);
                Destroy(this.gameObject);
                if (!joint) {
                    Debug.Log($"Joint on Block #{blockBehaviour?.transform.GetSiblingIndex()} ({blockBehaviour?.name}) with strength {strength} broke or was destroyed");
                }
                return;
            }

            self.SetPosition(0, joint.transform.TransformPoint(joint.anchor));
            self.SetPosition(1, joint.connectedBody.worldCenterOfMass);

            self.SetWidth(UpdatesAllBlockCollidersAndLines.GetLineWidth(joint.connectedBody.position), 0);

            if (ColliderscopeController.ShowJoints) {

                self.enabled = true;
                Color c;
                if (joint.breakForce == Mathf.Infinity) {
                    c = Color.cyan;
                }
                else if (joint.breakForce > 20000) {
                    c = Color.Lerp(Color.yellow, Color.green, (joint.breakForce - 20000f) / 40000f);
                }
                else {
                    c = Color.Lerp(Color.red, Color.yellow, (joint.breakForce - 1000f) / 20000f);
                }
                self.SetColors(c, c);
            }
            else {
                self.enabled = false;
                self.SetColors(Color.clear, Color.clear);
            }
        }
    }
}
