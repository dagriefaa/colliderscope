﻿using Modding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Colliderscope.Types {
    class ShowsFlamethrowerAoE : MonoBehaviour {

        FlamethrowerController self;

        Transform aoe;
        Renderer renderer;

        AbstractType environmentType;

        void Awake() {
            self = GetComponent<FlamethrowerController>();
            environmentType = ColliderscopeController.BlockColliderTypes["EnvironmentTrigger"];

            if (ReferenceMaster.activeMachineSimulating) {
                aoe = this.transform.FindChild("FireVis");
                renderer = aoe.GetComponent<Renderer>();
            }
            else {
                Modding.Serialization.ModCollider colliderVisGenerator;
                colliderVisGenerator = new Modding.Serialization.BoxModCollider() { Position = new Vector3(0, -1000, 0) };
                aoe = colliderVisGenerator.CreateVisual(transform);
                aoe.name = "FireVis";

                renderer = aoe.GetComponent<Renderer>();
                renderer.sharedMaterial = environmentType.ColliderMaterial;
            }
            self.RangeSlider.ValueChanged += OnValueChanged;
        }

        private void OnValueChanged(float value) {
            aoe.localPosition = self.fireController.overlapCenter + self.fireController.transform.localPosition;
            aoe.localScale = self.fireController.overlapSize;
        }

        void Update() {
            bool active = ColliderscopeController.ShowColliders
                && environmentType.Enabled
                && !(ReferenceMaster.activeMachineSimulating && !ColliderscopeController.ShowBlockSimulation)
                || BlockMapper.IsOpen && BlockMapper.CurrentInstance.Current == self;
            aoe.gameObject.SetActive(active && aoe.localScale.z > 0);

            renderer.gameObject.layer = ColliderscopeController.XRay ? 25 : 23;
        }
    }
}
