﻿using System.Collections.Generic;
using UnityEngine;

namespace Colliderscope.Types {
    public class ShowsSensorAoE : MonoBehaviour {

        SensorBlock self;

        Transform top;
        Transform bottom;
        Transform cylinder;

        List<Renderer> renderers = new List<Renderer>();

        AbstractType environmentType;

        void Awake() {
            self = GetComponent<SensorBlock>();
            environmentType = ColliderscopeController.BlockColliderTypes["EnvironmentTrigger"];

            if (ReferenceMaster.activeMachineSimulating) {
                top = ((GameObject)Instantiate((self.BuildingBlock as SensorBlock).sphereTop.gameObject, self.transform, false)).transform;
                bottom = ((GameObject)Instantiate((self.BuildingBlock as SensorBlock).sphereBottom.gameObject, self.transform, false)).transform;
                cylinder = ((GameObject)Instantiate((self.BuildingBlock as SensorBlock).cylinder.gameObject, self.transform, false)).transform;
            }
            else {
                top = self.sphereTop;
                bottom = self.sphereBottom;
                cylinder = self.cylinder;

                GameObject cylinder2 = (GameObject)Instantiate(cylinder.gameObject, cylinder.transform, false);
                cylinder2.transform.localScale = Vector3.one;
                cylinder2.transform.localEulerAngles = new Vector3(0, 90, 0);
                cylinder2.SetActive(true);
                cylinder2.GetComponent<Renderer>().sharedMaterial = environmentType.ColliderMaterial;
            }

            renderers.Add(top.GetComponent<Renderer>());
            renderers.Add(bottom.GetComponent<Renderer>());
            renderers.Add(cylinder.GetComponent<Renderer>());
            renderers.ForEach(x => { x.enabled = true; x.sharedMaterial = environmentType.ColliderMaterial; });
        }

        void Update() {

            bool active = ColliderscopeController.ShowColliders
                && environmentType.Enabled
                && !(ReferenceMaster.activeMachineSimulating && !ColliderscopeController.ShowBlockSimulation)
                || BlockMapper.IsOpen && BlockMapper.CurrentInstance.Current == self;
            top.gameObject.SetActive(active);
            bottom.gameObject.SetActive(active);
            cylinder.gameObject.SetActive(active);

            foreach (Renderer r in renderers) {
                r.gameObject.layer = ColliderscopeController.XRay ? 25 : 23;
            }

            if (active && !ReferenceMaster.activeMachineSimulating) {
                float radius = self.RadiusSllider.Value;
                float height = self.DistanceSlider.Value - radius * 2f;
                if (height < 0f)
                    height = 0f;
                Vector3 start = self.sensorPos.position + -self.transform.up * radius;
                Vector3 end = self.sensorPos.position + -self.transform.up * (height + radius);
                self.ShowOverlapCapsule(start, end, radius, height);
            }
        }
    }
}