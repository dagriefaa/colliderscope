﻿using Modding.Blocks;
using System.Collections;
using UnityEngine;

namespace Colliderscope.Types {
    public class ScalesSurfaceFragments : MonoBehaviour {

        bool isEntity = false;
        Transform target;

        void Start() {
            target = transform.FindChild(ColliderscopeController.COLLIDER_PARENT_NAME);
            if (target.lossyScale != Vector3.one || target.lossyScale != Vector3.zero) {
                target.localScale = new Vector3(
                    target.localScale.x == 0 ? 0f : 1f / target.lossyScale.x,
                    target.localScale.y == 0 ? 0f : 1f / target.lossyScale.y,
                    target.localScale.z == 0 ? 0f : 1f / target.lossyScale.z);
            }
        }

        void Update() {
            if (!target) {
                return;
            }
            if (!isEntity) {
                target.gameObject.SetActive(ColliderscopeController.ShowColliders
                    && (ColliderscopeController.ShowBlockSimulation || !ReferenceMaster.activeMachineSimulating));
            }
            else {
                target.gameObject.SetActive((ColliderscopeController.ShowEntitySimulation || !StatMaster.levelSimulating)
                    && ColliderscopeController.IsLevelEditor);
            }
        }
    }
}