﻿using Modding.Blocks;
using UnityEngine;

namespace Colliderscope.Types {
    /**
     * Regenerates surface colliders when surface is updated. Does not run in sim.
     */
    public class UpdatesSurfaceCollidersOnChange : MonoBehaviour {

        Transform simChild;
        Transform lastCollider;
        Block self;

        public bool setupComplete = false;

        void Start() {
            if (ReferenceMaster.activeMachineSimulating) {
                DestroyImmediate(this);
                return;
            }
            BuildSurface behaviour = GetComponent<BuildSurface>();
            self = Block.From(behaviour);
            simChild = behaviour.simColliderParent.transform;
        }

        public void FinishSetup() {
            setupComplete = true;
        }

        void LateUpdate() {
            if (!setupComplete) {
                return;
            }

            if (!lastCollider && !InputManager.LeftMouseButtonHeld() && simChild.childCount > 0) {
                ColliderscopeController.RemakeColliderVis(self);
                lastCollider = simChild.GetChild(0);
            }
        }
    }
}