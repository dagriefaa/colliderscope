using Modding;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Colliderscope {
    public class Mod : ModEntryPoint {

        public static GameObject Controller { get; private set; }
        public override void OnLoad() {
            Controller = GameObject.Find("ModControllerObject");
            if (!Controller) { UnityEngine.Object.DontDestroyOnLoad(Controller = new GameObject("ModControllerObject")); }

            Controller.AddComponent<ColliderscopeController>();

            if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) { Controller.AddComponent<Mapper>(); }
        }
        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

        /// <see href="https://stackoverflow.com/a/34006336" />
        public static int CustomHash(params int[] vals) {
            int hash = 1009;
            foreach (int i in vals) {
                hash = (hash * 9176) + i;
            }
            return hash;
        }

        public static float[] SerialiseColorToArray(Color c) {
            return new float[] { c.r, c.g, c.b, c.a };
        }

        public static Color DeserialiseColorFromArray(float[] c) {
            return new Color(c[0], c[1], c[2], c[3]);
        }
    }
}
