﻿using Colliderscope.Types;
using Modding.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Colliderscope {
    public class UpdatesSingleColliderVisuals : MonoBehaviour {

        public Collider Self { get; private set; } = null;
        public SaveableDataHolder Block { get; private set; } = null;

        Renderer[] renderers = null;

        public AbstractType VisType { get; private set; } = null;


        public bool Visible = true;
        public int Hash = default;

        Vector3 lastPosition = default;
        Vector3 lastScale = default;

        public static Queue<GameObject> TrashList = new Queue<GameObject>();

        public static UpdatesSingleColliderVisuals Make(GameObject self, Collider collider, SaveableDataHolder block, int hash) {
            UpdatesSingleColliderVisuals res = self.AddComponent<UpdatesSingleColliderVisuals>();
            res.Self = collider;
            res.Block = block;
            res.VisType = GetVisType(collider, block);
            res.Hash = hash;

            return res;
        }

        public static AbstractType GetVisType(Collider c, SaveableDataHolder b) {
            var types = (b is BlockBehaviour)
                ? ColliderscopeController.BlockColliderTypes
                : ColliderscopeController.EntityColliderTypes;
            return types.Values.FirstOrDefault(t => t.IsType(c, b)) ?? types[nameof(NonConnectableSolid)];
        }


        void Start() {
            if (transform.childCount == 4) { // sensor?
                DestroyImmediate(transform.GetChild(2).gameObject);
            }
            // load sim data
            if (!Block || !Self) {
                bool foundCollider = TracksCollidersOverInstantiation.Colliders.TryGetValue(Hash, out var collider);
                bool foundBlock = TracksCollidersOverInstantiation.Blocks.TryGetValue(Hash, out var block);
                if (!(foundCollider && foundBlock)) {
                    return;  // automatically destroyed on update
                }
                Self = collider;
                if (block is GenericEntity) {
                    Block = Modding.Levels.Entity.From(block.gameObject).SimEntity.Behaviour.InternalObject;
                }
                else {
                    Block = (block as BlockBehaviour).SimBlock;
                }
                if (!Block || !Self) {
                    return; // automatically destroyed on update
                }

                VisType = GetVisType(Self, Block);
            }
            if (VisType == null) { // automatically destroyed on update
                return;
            }
            renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers) {
                if (!r) {
                    continue;
                }
                r.enabled = false;
                r.sharedMaterial = VisType.ColliderMaterial;
                r.gameObject.layer = ColliderscopeController.XRay ? 25 : 23;
            }
            VisType.OnCreate(this);

        }

        void OnDestroy() {
            VisType?.OnDestroy(this);
        }

        void Update() {
            // destroy conditions
            if (!Block || !Self) {
                TrashList.Enqueue(gameObject);
                gameObject.SetActive(false);
                return;
            }

            // get active
            bool gameObjectEnabled = Self.gameObject.activeSelf && Self.gameObject.activeInHierarchy;
            bool colliderAlwaysDisabled = Block is GrabberBlock || Block is VacuumBlock;
            bool isActiveSurfaceAddPoint = Block is BuildSurface && VisType is ConnectableNonSolid
                && ((Block as BuildSurface).nodes.Length > 3 || (Block as BuildSurface).AddingPoints[3] != Self);
            bool isBuildDisabledSurfaceCollider = Block is BuildSurface && !ReferenceMaster.activeMachineSimulating
                && (VisType is SurfaceSolid || isActiveSurfaceAddPoint);

            bool active = VisType.Enabled && ((gameObjectEnabled && (Self.enabled || colliderAlwaysDisabled)) || isBuildDisabledSurfaceCollider);

            // update materials
            foreach (Renderer r in renderers) {
                if (!r) {
                    continue;
                }
                r.enabled = active && Visible;

                if (ColliderscopeController.XRay == (r.gameObject.layer == 23)) {
                    r.gameObject.layer = ColliderscopeController.XRay ? 25 : 23;
                }
            }

            transform.position = VisColliderPos;

            // update scale
            if (active && (lastPosition != VisColliderPos || lastScale != Self.transform.lossyScale)) {
                lastPosition = VisColliderPos;
                lastScale = Self.transform.lossyScale;

                if (Self is BoxCollider) {
                    BoxCollider boxCollider = Self as BoxCollider;
                    transform.localScale = Vector3.Scale(Self.transform.lossyScale, boxCollider.size);

                    if (Block is SpringReleaseBlock && transform.localScale == new Vector3(0.5f, 0, 0)) { // 1d add point???
                        transform.localScale = new Vector3(0.5f, 0.002f, 0.002f);
                    }
                    if (Block is GenericDraggedBlock) {
                        transform.rotation = Self.transform.rotation;
                    }
                }
                if (Self is SphereCollider) {
                    SphereCollider sphereCollider = Self as SphereCollider;
                    transform.localScale = Vector3.one * sphereCollider.radius * 2f * Mathf.Max(new[] {
                        Self.transform.lossyScale.x,
                        Self.transform.lossyScale.y,
                        Self.transform.lossyScale.z
                    });
                }
                if (Self is CapsuleCollider) {
                    CapsuleCollider capsuleCollider = Self as CapsuleCollider;
                    float height = Mathf.Abs(capsuleCollider.height * Self.transform.lossyScale[capsuleCollider.direction]);
                    float radius = capsuleCollider.radius * Mathf.Max(Self.transform.lossyScale[(capsuleCollider.direction + 1) % 3],
                        Self.transform.lossyScale[(capsuleCollider.direction + 2) % 3]);
                    float focalDistance = height > 2f * radius ? height - 2f * radius : 0;

                    Transform top = transform.GetChild(0);
                    Transform bottom = transform.GetChild(1);
                    Transform center = transform.GetChild(2);
                    top.localPosition = Vector3.forward * focalDistance / 2f;
                    bottom.localPosition = -Vector3.forward * focalDistance / 2f;
                    top.localScale = Vector3.one * radius * 2f;
                    bottom.localScale = Vector3.one * radius * 2f;
                    center.localScale = new Vector3(radius * 2f, focalDistance / 2f, radius * 2f);
                }
            }
        }

        Vector3 VisColliderPos {
            get {
                if (!Self)
                    return Vector3.zero;
                if (Self is BoxCollider)
                    return Self.transform.TransformPoint((Self as BoxCollider).center);
                if (Self is SphereCollider)
                    return Self.transform.TransformPoint((Self as SphereCollider).center);
                if (Self is CapsuleCollider)
                    return Self.transform.TransformPoint((Self as CapsuleCollider).center);
                return Self.transform.position;
            }
        }
    }
}