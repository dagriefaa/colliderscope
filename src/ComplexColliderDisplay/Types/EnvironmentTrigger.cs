﻿using UnityEngine;

namespace Colliderscope.Types {
    class EnvironmentTrigger : AbstractType {

        public override string Name => "EnvironmentTrigger";
        public override string Label => "Ψ ENV    ";

        public override Vector3 ButtonOffset => new Vector3(1, 6, 1);

        public override Color DefaultColor => Color.blue;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => true;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 2) || (c.gameObject.layer == 11) 
                || (c.gameObject.layer == 0 && b is GrabberBlock && c.GetComponent<JoinOnTriggerBlock>());
        }
    }
}
