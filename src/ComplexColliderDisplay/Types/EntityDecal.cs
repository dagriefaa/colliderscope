﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Colliderscope.Types {
    class EntityDecal : AbstractType {
        public override string Name => "EntityDecal";

        public override string Label => "□ OCCLUDE";

        public override Vector3 ButtonOffset => new Vector3(0, 0, 0);

        public override bool DefaultEnabled => false;

        public override Color DefaultColor => Color.white;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => true;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return b is LevelDecalObject || c.isTrigger;
        }
    }
}
