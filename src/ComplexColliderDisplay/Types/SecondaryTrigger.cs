﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Colliderscope.Types {
    class SecondaryTrigger : AbstractType {

        public override string Name => "SecondaryTrigger";
        public override string Label => "○ SECOND ";

        public override Vector3 ButtonOffset => new Vector3(1, 4, 1);

        public override Color DefaultColor => new Color(1f, 0.25f, 0f);

        public override bool IsDiffuse => false;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 22) && c.GetComponent<TriggerSetJointBase>() is TriggerSetJoint2;
        }

        public HashSet<UpdatesSingleColliderVisuals> triggers = new HashSet<UpdatesSingleColliderVisuals>();

        public override void OnCreate(UpdatesSingleColliderVisuals h) {
            triggers.Add(h);
        }
        public override void OnDestroy(UpdatesSingleColliderVisuals h) {
            triggers.Remove(h);
        }

        public override Action<Machine> OnUpdate => _ => {
            
            foreach (UpdatesSingleColliderVisuals h in triggers) {
                if (!h.Self) {
                    continue;
                }
                SphereCollider col = h.Self as SphereCollider;
                float scale = Mathf.Max(col.transform.lossyScale.x, col.transform.lossyScale.y, col.transform.lossyScale.z);
                Collider[] overlaps = Physics.OverlapSphere(col.transform.TransformPoint(col.center), col.radius * scale, 1 << 22);
                h.Visible = !overlaps.Any(x => x.GetComponent<TriggerSetJoint>());
            }
        };
    }
}
