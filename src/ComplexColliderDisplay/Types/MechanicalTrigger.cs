﻿using UnityEngine;

namespace Colliderscope.Types {
    class MechanicalTrigger : AbstractType {

        public override string Name => "MechanicalTrigger";
        public override string Label => "○ MECHN ";

        public override Vector3 ButtonOffset => new Vector3(2, 3, 1);

        public override Color DefaultColor => new Color(1f, 0.21f, 0.3f);

        public override bool IsDiffuse => false;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 22) && b.gameObject.CompareTag("MechanicalTag");
        }
    }
}
