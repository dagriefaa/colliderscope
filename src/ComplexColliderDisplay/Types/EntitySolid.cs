﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Colliderscope.Types {
    class EntitySolid : AbstractType {
        public override string Name => "EntitySolid";

        public override string Label => "■ SOLID  ";

        public override Vector3 ButtonOffset => new Vector3(2,0,0);

        public override bool DefaultEnabled => false;

        public override Color DefaultColor => new Color(1f, 0f, 1f);

        public override bool IsDiffuse => true;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return !(b is LevelDecalObject);
        }
    }
}
