﻿using UnityEngine;

namespace Colliderscope.Types {
    class SurfaceSolid : AbstractType {

        public override string Name => "SurfaceSolid";
        public override string Label => "■ SURFACE";

        public override Vector3 ButtonOffset => new Vector3(2, 1, 1);

        public override Color DefaultColor => new Color(1f, 0f, 1f);

        public override bool IsDiffuse => true;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 0) && (b is BuildSurface);
        }
    }
}
