﻿using UnityEngine;

namespace Colliderscope.Types {
    class ConnectableSolid : AbstractType {

        public override string Name => "ConnectableSolid";
        public override string Label => "■ C SOLID";

        public override Vector3 ButtonOffset => new Vector3(1, 0, 1);

        public override Color DefaultColor => Color.cyan;

        public override bool IsDiffuse => true;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 12 || c.gameObject.layer == 14) && !(c.isTrigger || b is SqrBalloonController);
        }
    }
}
