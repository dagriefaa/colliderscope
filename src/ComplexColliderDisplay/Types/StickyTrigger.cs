﻿using UnityEngine;

namespace Colliderscope.Types {
    class StickyTrigger : AbstractType {

        public override string Name => "StickyTrigger";
        public override string Label => "○ STICKY ";

        public override Vector3 ButtonOffset => new Vector3(1, 5, 1);

        public override Color DefaultColor => Color.yellow;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 22)
                && c.GetComponent<TriggerSetJointBase>() is TriggerSetJointBrace
                && c.name == "B";
        }
    }
}
