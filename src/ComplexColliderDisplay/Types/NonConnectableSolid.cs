﻿using UnityEngine;

namespace Colliderscope.Types {
    class NonConnectableSolid : AbstractType {

        public override string Name => "NonConnectableSolid";
        public override string Label => "■ N SOLID";

        public override Vector3 ButtonOffset => new Vector3(1, 1, 1);

        public override Color DefaultColor => new Color(1f, 0f, 1f);

        public override bool IsDiffuse => true;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 25 || c.gameObject.layer == 0 || c.gameObject.layer == 15)
                && !(b is SliderCompress && c.gameObject == b.gameObject)
                && !(b is BuildSurface)
                && !(b is GrabberBlock && c.GetComponent<JoinOnTriggerBlock>());
        }
    }
}
