﻿using Modding;
using System;
using UnityEngine;

namespace Colliderscope.Types {
    public abstract class AbstractType {

        public abstract string Name { get; }
        public abstract string Label { get; }
        public abstract Vector3 ButtonOffset { get; }
        public virtual bool DefaultEnabled { get; } = true;
        public abstract Color DefaultColor { get; }
        public abstract bool IsDiffuse { get; }
        public abstract bool IsWireframe { get; }

        bool? _enabled = null;
        public bool Enabled {
            get {
                if (_enabled == null) {
                    if (Configuration.GetData().HasKey(Name)) {
                        Enabled = Configuration.GetData().ReadBool(Name);
                    }
                    else {
                        Enabled = DefaultEnabled;
                    }
                }
                return (bool)_enabled;
            }
            set {
                _enabled = value;
                Configuration.GetData().Write(Name, value);
            }
        }
        private string ColorID => Name + "RGBA";
        private Color? _colliderColor = null;
        public Color ColliderColor {
            get {
                if (_colliderColor == null) {
                    if (Configuration.GetData().HasKey(ColorID)) {
                        ColliderColor = Mod.DeserialiseColorFromArray(Configuration.GetData().ReadFloatArray(ColorID));
                    }
                    else {
                        ColliderColor = DefaultColor * new Color(1, 1, 1,
                            IsDiffuse
                                ? DiffuseOpacity
                                : (IsWireframe) ? WireframeOpacity : AdditiveOpacity
                        );
                    }
                }
                return (Color)_colliderColor;
            }
            set {
                _colliderColor = value;
                Configuration.GetData().Write(ColorID, Mod.SerialiseColorToArray(value));
                UpdateMaterial();
            }
        }
        private Material _colliderMaterial = null;
        public Material ColliderMaterial {
            get {
                if (!_colliderMaterial) {
                    InitMaterials();
                }
                return _colliderMaterial;
            }
            private set {
                _colliderMaterial = value;
            }
        }

        const string DiffuseShader = "Transparent/Diffuse";
        const string AdditiveShader = "Particles/Alpha Blended";
        const float DiffuseOpacity = 0.2f;
        const float AdditiveOpacity = 0.05f;
        const float WireframeOpacity = 1f;

        static Texture FaceTexture;
        static Texture WireframeTexture;

        protected virtual void InitMaterials() {

            if (!FaceTexture || !WireframeTexture) {
                GameObject buffer = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                FaceTexture = buffer.GetComponent<Renderer>().material.mainTexture;
                UnityEngine.Object.Destroy(buffer);

                buffer = new Modding.Serialization.BoxModCollider().CreateVisual(Mod.Controller.transform).gameObject;
                WireframeTexture = buffer.GetComponent<Renderer>().material.mainTexture;
                UnityEngine.Object.Destroy(buffer);
            }

            if (!_colliderMaterial) {
                ColliderMaterial = new Material(Shader.Find(IsDiffuse ? DiffuseShader : AdditiveShader));
            }
            UpdateMaterial();
        }

        private void UpdateMaterial() {
            ColliderMaterial.mainTexture = (IsWireframe) ? WireframeTexture : FaceTexture;
            ColliderMaterial.color = ColliderColor;
            ColliderMaterial.SetColor("_TintColor", ColliderColor);
        }

        public abstract bool IsType(Collider c, SaveableDataHolder b);

        public virtual void OnCreate(UpdatesSingleColliderVisuals h) {
        }
        public virtual void OnDestroy(UpdatesSingleColliderVisuals h) {
        }
        public virtual Action<Machine> OnUpdate { get; } = null;
    }
}
