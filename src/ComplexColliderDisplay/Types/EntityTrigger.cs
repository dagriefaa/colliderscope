﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Colliderscope.Types {
    class EntityTrigger : AbstractType {
        public override string Name => "EntityInsignia";

        public override string Label => "□ TRIGGER";

        public override Vector3 ButtonOffset => new Vector3(1,0,0);

        public override bool DefaultEnabled => false;

        public override Color DefaultColor => Color.blue;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => true;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return b is InsigniaTrigger;
        }
    }
}
