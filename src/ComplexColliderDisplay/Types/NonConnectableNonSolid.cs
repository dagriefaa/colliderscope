﻿using UnityEngine;

namespace Colliderscope.Types {
    class NonConnectableNonSolid : AbstractType {

        public override string Name => "NonConnectableNonSolid";
        public override string Label => "□ OCCLUDE";

        public override Vector3 ButtonOffset => new Vector3(1, 7, 1);

        public override Color DefaultColor => Color.white;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => true;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 21 || c.gameObject.layer == 16 || c.gameObject.layer == 27 || c.gameObject.layer == 24)
                || (c.gameObject.layer == 0 && b is SliderCompress && c.gameObject == b.gameObject);
        }
    }
}
