﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Colliderscope.Types {
    class PrimaryTrigger : AbstractType {

        public override string Name => "PrimaryTrigger";
        public override string Label => "○ FIRST  ";

        public override Vector3 ButtonOffset => new Vector3(1, 3, 1);

        public override Color DefaultColor => Color.red;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => false;

        public HashSet<UpdatesSingleColliderVisuals> triggers = new HashSet<UpdatesSingleColliderVisuals>();

        public override bool IsType(Collider c, SaveableDataHolder b) {
            if (c.gameObject.layer == 22) {
                return c.GetComponent<TriggerSetJointBase>()
                    || (b is FixedCameraBlock && c.GetComponent<TriggerSetParent1>());
            }
            return c.GetComponent<TriggerSetJointSurface>();
        }

        public override void OnCreate(UpdatesSingleColliderVisuals h) {
            if (ReferenceMaster.activeMachineSimulating) {
                return;
            }
            if (h.Block is BraceCode) {
                (h.Block as BraceCode).Sliders.ToList().ForEach(x => x.ValueChanged += y => OnUpdateOne(h));
            }
            OnUpdateOne(h);
        }

        public override Action<Machine> OnUpdate => _ => {
            foreach (UpdatesSingleColliderVisuals h in triggers) {
                OnUpdateOne(h);
            }
        };

        private void OnUpdateOne(UpdatesSingleColliderVisuals h) {
            if (ReferenceMaster.activeMachineSimulating || !h || !h.Block) {
                return;
            }
            h.Visible = !(h.Block is BraceCode
                && BraceCode.BraceType(h.transform.localScale, (h.Block as BraceCode).cylinder.localScale.y) == BraceState.Cube);

        }
    }
}
