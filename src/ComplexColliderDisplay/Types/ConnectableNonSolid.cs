﻿using UnityEngine;

namespace Colliderscope.Types {
    class ConnectableNonSolid : AbstractType {

        public override string Name => "ConnectableNonSolid";
        public override string Label => "□ ADD PT ";

        public override Vector3 ButtonOffset => new Vector3(1, 2, 1);

        public override Color DefaultColor => Color.green;

        public override bool IsDiffuse => false;

        public override bool IsWireframe => false;

        public override bool IsType(Collider c, SaveableDataHolder b) {
            return (c.gameObject.layer == 12 || c.gameObject.layer == 14) && (c.isTrigger || b is SqrBalloonController);
        }
    }
}
