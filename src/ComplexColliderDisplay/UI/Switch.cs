﻿using System;
using UnityEngine;

namespace Colliderscope.UI {
    public class Switch : ClickBehaviour {

        public static Material redMaterial, darkMaterial;
        public Func<bool> get;
        public Action<bool> set;

        Renderer r;

        public void Start() {
            UpdateMaterial();
        }

        public void UpdateMaterial() {
            if (!r) r = GetComponent<Renderer>();
            r.sharedMaterial = get() ? redMaterial : darkMaterial;
        }

        public override void OnClicked() {
            set(!get());
            UpdateMaterial();
        }
    }
}

