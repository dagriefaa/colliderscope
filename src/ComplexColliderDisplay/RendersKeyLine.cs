﻿using cakeslice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Colliderscope {
    class RendersKeyLine : MonoBehaviour {

        public BlockBehaviour from;
        public BlockBehaviour to;
        public bool inBackground;

        LineRenderer self;
        Outline fromOutline;
        Outline toOutline;

        static Camera mainCamera;

        Dictionary<BlockType, Color> lineColors = new Dictionary<BlockType, Color>() {
            {BlockType.Altimeter, Color.green },
            {BlockType.Anglometer, new Color(1f, 0.5f, 0f) },
            {BlockType.LogicGate, Color.blue },
            {BlockType.Speedometer, Color.red },
            {BlockType.Sensor, Color.cyan },
            {BlockType.Timer, Color.magenta }
        };

        Dictionary<BlockType, Material> offMaterials; // init Awake()

        void Awake() {
            offMaterials = new Dictionary<BlockType, Material>() {
                { BlockType.Altimeter, new Material(Shader.Find("Particles/Alpha Blended")) },
                { BlockType.Anglometer, new Material(Shader.Find("Particles/Alpha Blended")) },
                { BlockType.LogicGate, new Material(Shader.Find("Particles/Alpha Blended")) },
                { BlockType.Speedometer, new Material(Shader.Find("Particles/Alpha Blended")) },
                { BlockType.Sensor, new Material(Shader.Find("Particles/Alpha Blended")) },
                { BlockType.Timer, new Material(Shader.Find("Particles/Alpha Blended")) }
            };
        }

        void Start() {
            if (!mainCamera) {
                mainCamera = Camera.main;
            }

            self = GetComponent<LineRenderer>();
            offMaterials.TryGetValue((BlockType)to.Prefab.ID, out Material offMaterial);
            if (!offMaterial) {
                //Debug.Log($"Did not find key line material for block type {to?.Prefab?.name}", this);
                return;
            }
            self.sharedMaterial = offMaterial;

            fromOutline = from.GetComponentInChildren<Outline>();
            toOutline = to.GetComponentInChildren<Outline>();

            if (ReferenceMaster.activeMachineSimulating) {
                if (!self.enabled) {
                    Destroy(this.gameObject);
                    return;
                }

                RendersKeyLine buildLine = GetComponentInParent<BlockBehaviour>().BuildingBlock.transform.FindChild("Joints Vis")
                    .GetChild(this.transform.GetSiblingIndex()).GetComponent<RendersKeyLine>();
                this.from = buildLine.from.SimBlock;
                this.to = buildLine.to.SimBlock;
            }
            else {
                if (!to || !from) {
                    Destroy(this.gameObject);
                    return;
                }
            }
        }

        void Update() {

            if (!to || !from || !self || !fromOutline || !toOutline) {
                Destroy(this.gameObject);
                return;
            }

            bool active = (ReferenceMaster.activeMachineSimulating && false)
                || (!ReferenceMaster.activeMachineSimulating && (fromOutline.enabled || toOutline.enabled));

            float alpha = (inBackground) ? 0.2f : 1f;
            if ((OverviewBlockMapper.IsOpen || ReferenceMaster.activeMachineSimulating) && !active) {
                alpha = Mathf.Max(alpha * 0.3f, 0.15f);
            }

            if (((BlockMapper.IsOpen && active) || OverviewBlockMapper.IsOpen)) {
                Color c = lineColors[(BlockType)to.Prefab.ID];
                self.SetColors(
                    new Color(c.r, c.g, c.b, alpha),
                    new Color(c.r + 0.8f, c.g + 0.8f, c.b + 0.8f, alpha)
                    );
                self.enabled = true;
            }
            else {
                self.SetColors(Color.clear, Color.clear);
                self.enabled = false;
            }

            self.SetPosition(0, from.transform.position);
            self.SetPosition(1, to.transform.position);
            Vector3 closest = ((mainCamera.transform.position - from.transform.position).sqrMagnitude > (mainCamera.transform.position - to.transform.position).sqrMagnitude)
                ? to.transform.position
                : from.transform.position;
            float width = UpdatesAllBlockCollidersAndLines.GetLineWidth(closest, max: int.MaxValue);
            self.SetWidth(width * 0.5f, width * 0.5f);
        }
    }
}
