[img=https://i.imgur.com/6wlh6Rt.png][/img]
                        [b][url=https://besiege.fandom.com/wiki/Colliders]Collider Wiki Page[/url][/b]  |  [b][url=https://gitlab.com/dagriefaa/colliderscope]GitLab Repository[/url][/b]

[img]https://i.imgur.com/vbbc26C.gif[/img]

A more detailed collider mod.
It displays the colliders and connection points of blocks (details are below). It's useful for advanced building, where precision is crucial.

(One feature of this mod is that you can see where to select bracecubes!)

[h1]Collider Summary[/h1]
💠 [url=https://store.steampowered.com][b][C SOLID] - Connectable Collider[/b][/url] Has collision; connects triggers
🟣 [url=https://store.steampowered.com][b][N SOLID] - Non-Connectable Collider[/b][/url] Has collision; does not connect triggers.
🟢 [url=https://store.steampowered.com][b][ADD PT] - Add Point[/b][/url] No collision; connects triggers.

🔴 [url=https://store.steampowered.com][b][FIRST] - Primary/Bottom Joint[/b][/url] Connects to ONE {Connectable Solid, Add Point} intersecting it*
🟠 [url=https://store.steampowered.com][b][SECOND] - Secondary/Top Joint[/b][/url] Connects to ONE {Connectable Solid, Add Point} intersecting it*; [b]does not connect to anything if it intersects any primary or mechanical[/b]; generally stronger than other trigger types
⚪ [url=https://store.steampowered.com][b][MECHN] - Mechanical Joint[/b][/url] Connects to ONE {Connectable Solid, Add Point} intersecting it*; identical to primary triggers, except [b]primary triggers intersecting it will not connect to its block[/b]
🟡 [url=https://store.steampowered.com][b][STICKY] - Multi Joint[/b][/url] Connects to ALL {Connectable Solid, Add Point} intersecting it

🔳 [url=https://store.steampowered.com][b][OCCLUDE] - Selection Occluder[/b][/url] No collision; exists only to be selected (e.g. braces)
🟦 [url=https://store.steampowered.com][b][ENV] - Environment Trigger[/b][/url] Triggers which are directly controlled by the player (e.g. grabbers, vacuums, sensors)

* see following section

[h1]Connection Dynamics[/h1]
Joints will usually attach to the rightmost object intersecting them (X+ axis). 
If there are multiple objects with the same X position, they will attach to the first placed object.

Blocks whose primary triggers intersect those of joint/mechanical blocks (e.g. wheels, cogs, steering hinges, swivels, pistons, etc.) will not connect to those joint/mechanical blocks. Normal hinges are an exception to this.

There is some unexplained variation in connection rigidity and strength beyond this, which can be observed with certain toring-based setups. The order in which blocks were placed has some effect on this, but the exact rules are unknown.

[h1]Joint Connection Display[/h1]
[img=https://i.imgur.com/sN7OGPS.png][/img]

[url=https://store.steampowered.com][b][JOINTS][/b][/url] - show connections between blocks [b]in simulation only[/b].
The larger end of the line extends from the location of the trigger, and the smaller end from the center of the block it connects to.
The colour of the lines indicates the strength of the joint, blending between:
- 🔴 Red = 1000 and below
- 🟡 Yellow = 20000
- 🟢 Green = 60000 and above
- 💠 Cyan = Unbreakable

[h1]Mod Notes[/h1]
[olist]
    [*] Zero scale collider displays are inaccurate, unfortunately (due to how I've implemented this)
[/olist]

[h1]Credits[/h1]
ITR, for making the first collider mod (which was instrumental in testing this mod).
Impossiblin, for the punny name.