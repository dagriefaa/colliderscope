<div align="center">
<img src="header.png" alt="AC HUD MOD" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1865492589<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1865492589?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1865492589?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1865492589?color=%231b2838&logo=steam&style=for-the-badge)
</div>

Colliderscope is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds a collider and trigger display for machine blocks, which is important as the block colliders are not the same as the visual appearance of the blocks (and can behave in non-intuitive ways). The mod distinguishes between various different types of colliders and triggers, each with different behaviour.

This is a must-have if you decide you want to get into scaling blocks, but is also extremely useful for vanilla advanced building.

## Features
- Displays colliders of blocks and level entities
- Displays connections between blocks during simulation
- Distinguishes between most known types of collider, and can enable/disable visibility for each of them
- Toggleable x-ray and simulation modes
- Asynchronous generation ensures minimal lag when loading machines (or moving Build Surfaces!)
- Configurable collider type colours

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
The collider display option for blocks is found in the quick menu (the cog in the top right of the UI). When turned on, other options (like collider type visibility, x-ray, and simulation) are configurable. <br>
The joint display option is right next to it. It only works in simulation, as that's the only reliable way to figure out what will connect to what.

The collider display options for level entities are at the bottom of the Level Editor window.

Additional usage instructions can be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=1865492589). <br>
[Details on the various collider types can be found on the Besiege Wiki.](https://besiege.fandom.com/wiki/Colliders)

## Status
This iteration of Colliderscope is complete and no further development will occur. It has been superseded by [Colliderscope 2](https://gitlab.com/dagriefaa/colliderscope-2).

## Images
<img src="https://i.imgur.com/vbbc26C.gif" alt="showcase" width="500"/>
<img src="https://i.imgur.com/sN7OGPS.png" alt="entities" width="500"/>
